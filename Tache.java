package gestionnaireTache;

import java.util.Date;

public class Tache {
	
	private int identifiant;
	static int dernierId = 1;
	private String titre;
	public enum ETAT{
		EN_COURS, TERMINE, PREVU
	}
	private ETAT etat;
	private Date date_creation;
	
	
	// CONSTRUCTEURS
	public Tache(String titre) {
		super();
		this.titre = titre;
		this.identifiant = dernierId;
		this.etat = ETAT.PREVU;
		this.date_creation = new Date();
		dernierId++;
	}

	public Tache(String titre, ETAT etat) {
		super();
		this.identifiant = dernierId;
		this.titre = titre;
		this.etat = etat;
		this.date_creation = new Date();
		dernierId++;
	}

	public Tache(String titre, ETAT etat, Date date_creation) {
		super();
		this.identifiant = dernierId;
		this.titre = titre;
		this.etat = etat;
		this.date_creation = date_creation;
		dernierId++;
	}

	
	// GETTERS AND SETTERS
	public int getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}

	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}

	public ETAT getEtat() {
		return etat;
	}
	public void setEtat(ETAT etat) {
		this.etat = etat;
	}

	
	public Date getDate_creation() {
		return date_creation;
	}
	public void setDate_creation(Date date_creation) {
		this.date_creation = date_creation;
	}

	@Override
	public String toString() {
		return "Tache [identifiant=" + identifiant + ", titre=" + titre + ", etat=" + etat.toString() + ", date_creation="
				+ date_creation + "]";
	}

	public Tache(int identifiant, String titre, ETAT etat, Date date_creation) {
		super();
		this.identifiant = identifiant;
		this.titre = titre;
		this.etat = etat;
		this.date_creation = date_creation;
	}

	
//    public String toString(){
//        String reponse = "";
//        reponse = "Tache n° : "+this.identifiant;
//        reponse += "\nTitre : "+this.titre;
//        reponse += "\nEtat : "+this.etat;
//        reponse += "\nDate de creation: "+this.date_creation;
//        return reponse;
//    }
}
