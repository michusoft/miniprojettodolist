package gestionnaireTache;

import java.util.Date;
import java.util.Scanner;

import gestionnaireTache.Tache.ETAT;

public class GestionTache {

	private static GestionnaireTache gestionnaire = new GestionnaireTache(5);
	private static int compte=0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		int choix;

		System.out.println("");
		System.out.println("                               |**********   M E N U  -  T O D O L I S T  *********|");
		System.out.println("                               |                                                   |");
		System.out.println("                               | 1 - AJOUTER                                       |");
		System.out.println("                               | 2 - MODIFIER                                      |");
		System.out.println("                               | 3 - SUPPRIMER                                     |");
		System.out.println("                               | 4 - LISTER TOUTES LES TACHES                      |");
		System.out.println("                               | 5 - LISTER LES TACHES ENCOURS                     |");
		System.out.println("                               | 6 - LISTER LES TACHES PREVUES                     |");
		System.out.println("                               | 7 - LISTER LES TACHES TERMINEES                   |");
		System.out.println("                               | 8 - EXIT                                          |");
		System.out.println("                               |___________________________________________________|");
		


//		System.out.print("Veuillez saisir le nombre initial de taches : ");
//		nombre = sc.nextInt();
		do {
			System.out.println("");
			System.out.print("Veuillez choisir une des opérations dans le menu (1 - 8) : ");
			choix = sc.nextInt();
			
			choix_menu(choix);
		} while (choix != 8);
		
		
	}

	public static void choix_menu(int choix){

		Scanner scan = new Scanner(System.in);
		switch (choix) {
		case 1: // OK - design a parfaire
			if(compte < gestionnaire.taches.length) {
				System.out.println("OPERATION : AJOUTER TACHE");
				System.out.println(" ");
				java.util.Scanner entree = new java.util.Scanner(System.in);
				String titre, etat_saisi;
				ETAT etat = null;
				Date date_crea;
				
				do {
					System.out.print("Veuillez saisir l'etat : ");
					etat_saisi = scan.next();
				} while ((!etat_saisi.equalsIgnoreCase("EN_COURS") && !etat_saisi.equalsIgnoreCase("PREVU") && !etat_saisi.equalsIgnoreCase("TERMINE")));
					
				System.out.println("");
				
				System.out.print("Veuillez saisir le titre : ");
				titre = entree.nextLine();
	
				System.out.println("");
				date_crea = new Date();
				System.out.println("");
				if(etat_saisi.equalsIgnoreCase("EN_COURS"))
					etat = ETAT.EN_COURS;
				else if(etat_saisi.equalsIgnoreCase("PREVU"))
					etat = ETAT.PREVU;
				else if(etat_saisi.equalsIgnoreCase("TERMINE"))
					etat = ETAT.TERMINE;
				
				Tache tache1 = new Tache(titre, etat, date_crea);
				gestionnaire.ajouter(tache1);
				compte ++;
			}
			else
				System.out.println("Liste de taches pleine");
			break;
			case 2:
				System.out.println("OPERATION : MODIFIER TACHE");
				System.out.println(" ");
				int id ;
				System.out.print("Veuillez saisir l'identifiant de la tache à modifier : ");
				id = scan.nextInt();
				java.util.Scanner entree2 =   new java.util.Scanner(System.in);
				String titre2, etat_saisi2;
				ETAT etat2 = null;
				
				do {
					System.out.println("");
					System.out.print("Veuillez saisir le nouvel etat: ");
					etat_saisi2 = scan.next();
				} while ((!etat_saisi2.equalsIgnoreCase("EN_COURS") && !etat_saisi2.equalsIgnoreCase("PREVU") && !etat_saisi2.equalsIgnoreCase("TERMINE")));
					
				System.out.println("");
				
				System.out.print("Veuillez saisir le nouveau titre : ");
				titre2 = entree2.nextLine();
	
				System.out.println("");
				if(etat_saisi2.equalsIgnoreCase("EN_COURS"))
					etat2 = ETAT.EN_COURS;
				else if(etat_saisi2.equalsIgnoreCase("PREVU"))
					etat2 = ETAT.PREVU;
				else if(etat_saisi2.equalsIgnoreCase("TERMINE"))
					etat2 = ETAT.TERMINE;
	
				Tache tache2 = new Tache(id,titre2, etat2, new Date());
				
				System.out.println("");
				Tache[] tachesRES = gestionnaire.taches;
				Boolean test = false;
	
				for (int index = 0; index < tachesRES.length; index++) {
					if(tachesRES[index].getIdentifiant() == id) {
						test = true;
					}
				}
				if(test)
					gestionnaire.modifier(tache2);
				else
					System.out.println("Tache a modifier introuvable");
			
			break;
		case 3: // OK
			System.out.println("OPERATION : SUPPRIMER TACHE");
			System.out.println(" ");
			int id3 ;
			System.out.print("Veuillez saisir l'identifiant de la tache à supprimer : ");
			id3 = scan.nextInt();
			gestionnaire.supprimer(id3);
			break;
		case 4: // OK
			System.out.println("OPERATION : LISTER TOUTES LES TACHES");
			System.out.println(" ");
			Tache[] taches = gestionnaire.lister();
			for (int index = 0; index < taches.length; index++) {
				System.out.println(taches[index].toString());
				System.out.println();
			}
			break;
		case 5:  // OK
			System.out.println("OPERATION : LISTER LES TACHES ENCOURS");
			System.out.println(" ");
			Tache[] taches5 = gestionnaire.taches;
			
			for (int index = 0; index < taches5.length; index++) {
				if(taches5[index].getEtat() == ETAT.EN_COURS) {
					System.out.println(taches5[index].toString());
					System.out.println("");
				}
			}
			break;
		case 6:  // OK
			System.out.println("OPERATION : LISTER LES TACHES PREVUES");
			System.out.println(" ");
			Tache[] taches6 = gestionnaire.taches;
			
			for (int index = 0; index < taches6.length; index++) {
				if(taches6[index].getEtat() == ETAT.PREVU) {
					System.out.println(taches6[index].toString());
					System.out.println("");
				}
			}
			break;
		case 7:  // OK
			System.out.println("OPERATION : LISTER LES TACHES TERMINEES");
			System.out.println(" ");
			Tache[] taches7 = gestionnaire.taches;
			
			for (int index = 0; index < taches7.length; index++) {
				if(taches7[index].getEtat() == ETAT.TERMINE) {
					System.out.println(taches7[index].toString());
					System.out.println("");
				}
			}
			break;
		case 8:  // OK
			System.out.println("AU REVOIR");
			break;

		default:
			System.out.println("Vous avez saisi : " + choix+ "\nVeuillez choisir entre 1 et 7 !!! ");
			break;
		}
	}

}
             