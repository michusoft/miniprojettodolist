package gestionnaireTache;

import java.util.Arrays;

public class GestionnaireTache {
	int nombreTache, indice = 0;
	Tache taches[] = new Tache[nombreTache];
	
	
	public GestionnaireTache(int nombreMax) {
		super();
		this.nombreTache = 0;
		this.taches = new Tache[nombreMax];
	}
	
	
	public boolean ajouter(Tache tache) {
		if(indice < taches.length) {
			taches[indice] = tache;
			nombreTache ++;
			indice++;
			return true;
		}else
			return false;
	}
	

	public boolean modifier(Tache tache) {
		boolean resultat = false;
		for (int index = 0; index < nombreTache; index++) {
			if(taches[index].getIdentifiant() == tache.getIdentifiant()) {
				taches[index].setDate_creation(tache.getDate_creation());
				taches[index].setEtat(tache.getEtat());
				taches[index].setTitre(tache.getTitre());
				resultat = true;
			}
		}
		return resultat;
	}
	
	
	public boolean supprimer(int id) {
		Tache[] taches_tampon = null;
		boolean retour = false;
		
		for (int i = 0; i < nombreTache; i++) {
            if(taches[i].getIdentifiant() == id){
                taches_tampon = new Tache[taches.length - 1];
                for(int index = 0; index < i; index++){
                    taches_tampon[index] = taches[index];
                }
                for(int j = i; j < taches.length - 1; j++){
                    taches_tampon[j] = taches[j+1];
                }
                retour = true;
                taches = taches_tampon;
                nombreTache--;
                indice--;
                break;
            }
        }
		System.out.println("Suppression de la tache id = "+id+"\nLe reste des taches est \n"+ Arrays.toString(taches));
		return retour;
		
	}
	
	public Tache[] lister() {
		
		return taches;
		
	}
	
	public Tache[] lister(String etat) {
		Tache[] taches_retour = null;
		int j = 0;

		for (int index = 0; index < taches.length; index++) {
			if(taches[index].getEtat().toString().equalsIgnoreCase(etat)) {
				taches_retour[j] = taches[index];
				j++;
			}
		}
		return taches_retour;
		
	}
}
